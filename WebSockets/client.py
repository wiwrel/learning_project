import socket
import time


def connect_to_server():
    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client_socket.connect(('localhost', 8000))
    ws_handshake = False

    handshake_request_data = [
        "GET / HTTP/1.1",
        "Upgrade: websocket",
        "Connection: Upgrade",
        "Sec-WebSocket-Key: dGhlIHNhbXBsZSBub25jZQ==",
        "Sec-WebSocket-Version: 13",
        "\r\n"
    ]

    handshake_request = "\r\n".join(handshake_request_data)

    while not ws_handshake:
        client_socket.send(handshake_request.encode('utf-8'))
        response = client_socket.recv(1024)

        if response:
            print(f"Server response: {response.decode('utf-8')}")
            ws_handshake = True
        else:
            print("Waiting for handshake...")
            continue

        time.sleep(3)

    while True:
        message = input("Enter a message (or 'quit' to exit): ")
        if message == 'quit':
            break

        client_socket.sendall(message.encode('utf-8'))

        response = client_socket.recv(1024)
        print(f"Server response: {response.decode('utf-8')}")

    client_socket.close()


if __name__ == '__main__':
    connect_to_server()
