import socket
import logging
import select
import base64
import hashlib

logger = logging.getLogger(__name__)

WEBSOCKET_SECRET = "258EAFA5-E914-47DA-95CA-C5AB0DC85B11"


def websocket_handshake(handshake_data):
    if not handshake_data:
        return

    headers = handshake_data.decode('utf-8').split('\r\n')
    sec_websocket_key = None

    for header in headers:
        if header.startswith('Sec-WebSocket-Key:'):
            sec_websocket_key = header.split(':')[1].strip()
            break

    if not sec_websocket_key:
        return

    magic_string = WEBSOCKET_SECRET
    response_key = base64.b64encode(hashlib.sha1(
        (sec_websocket_key + magic_string).encode(
            'utf-8')).digest()).decode('utf-8')

    response_headers = [
        "HTTP/1.1 101 Switching Protocols",
        "Upgrade: websocket",
        "Connection: Upgrade",
        f"Sec-WebSocket-Accept: {response_key}",
        "\r\n"
    ]

    return "\r\n".join(response_headers)


def accept_connection(sock):
    print('Before accepting connections')
    client_socket, addr = sock.accept()
    print('Got connection from: {}'.format(addr))
    return client_socket


def send_message(sock):
    print('Before receiving data')
    request = sock.recv(4096)

    if request:
        print('Sending request: {}'.format(request))
        response = f'Your request {request.decode()}'.encode()
        sock.send(response)
        print('Sent response: {}'.format(response))
        return True

    print('Empty response')
    return False


def websocket_loop(sock):
    handshake_data = sock.recv(1024)

    handshake_response = websocket_handshake(handshake_data)

    if handshake_response:
        sock.sendall(handshake_response.encode())
        print('Websocket handshake complete')
        return True

    return False


def start_server():
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    server_socket.bind(('0.0.0.0', 8080))
    server_socket.listen()
    print('Listening on port 8080')
    ws_handshake_log = {}
    readable_sockets = {server_socket}

    while True:
        readable, *_ = select.select(readable_sockets, set(), set())

        for sock in readable:
            if sock == server_socket:
                client_socket = accept_connection(sock)
                readable_sockets.add(client_socket)
            else:
                if not ws_handshake_log.get(sock):
                    handshake_result = websocket_loop(sock)
                    if not handshake_result:
                        readable_sockets.remove(sock)
                        sock.close()
                    else:
                        ws_handshake_log.setdefault(sock, handshake_result)
                else:
                    result = send_message(sock)

                    if not result:
                        readable_sockets.remove(sock)
                        sock.close()


if __name__ == '__main__':
    start_server()
