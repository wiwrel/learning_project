import socket
import logging
import base64
import hashlib

logger = logging.getLogger(__name__)

WEBSOCKET_SECRET = "258EAFA5-E914-47DA-95CA-C5AB0DC85B11"

PORT = 8000


def websocket_handshake(handshake_data):
    if not handshake_data:
        return

    headers = handshake_data.decode('utf-8').split('\r\n')
    sec_websocket_key = None

    for header in headers:
        if header.startswith('Sec-WebSocket-Key:'):
            sec_websocket_key = header.split(':')[1].strip()
            break

    if not sec_websocket_key:
        return

    magic_string = WEBSOCKET_SECRET
    response_key = base64.b64encode(hashlib.sha1(
        (sec_websocket_key + magic_string).encode(
            'utf-8')).digest()).decode('utf-8')

    response_headers = [
        "HTTP/1.1 101 Switching Protocols",
        "Upgrade: websocket",
        "Connection: Upgrade",
        f"Sec-WebSocket-Accept: {response_key}",
        "\r\n"
    ]

    return "\r\n".join(response_headers)


def start_server():
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    server_socket.bind(('0.0.0.0', PORT))
    server_socket.listen()
    print(f'Listening on port {PORT}')
    ws_handshake_log = {}

    while True:
        print('Before accepting connections')
        client_socket, addr = server_socket.accept()
        print('Got connection from: {}'.format(addr))

        while True:

            if not ws_handshake_log.get(client_socket):
                handshake_data = client_socket.recv(1024)

                handshake_response = websocket_handshake(handshake_data)

                if not handshake_response:
                    client_socket.close()
                else:
                    client_socket.sendall(handshake_response.encode())
                    print('Websocket handshake complete')
                    ws_handshake_log.setdefault(client_socket, True)
            else:
                print('Before receiving data')
                request = client_socket.recv(4096)

                if not request:
                    print('Connection closed')
                    break
                else:
                    print('Sending request: {}'.format(request))
                    response = f'Your request {request.decode()}'.encode()
                    client_socket.send(response)
                    print('Sent response: {}'.format(response))

        client_socket.close()


if __name__ == '__main__':
    start_server()
