import requests
import gzip


def accept_encoding_example(url: str):
    headers = {
        'Accept-Encoding': 'gzip, deflate',
    }

    response = requests.get(url, headers=headers)

    print(response.text)


def content_encoding_example(url_data: str):
    data = gzip.compress(b'{"key": "value"}')
    headers = {
        'Content-Encoding': 'gzip',
        'Content-Type': 'application/json',
    }

    response = requests.post(url_data, headers=headers, data=data)

    print(response.text)


def user_agent_accept_language_example(url: str):
    headers = {
        'User-Agent': 'MyApp/1.0 (Linux; U; Android 8.1)',
        'Accept-Language': 'en-US,en;q=0.5',
    }

    response = requests.get(url, headers=headers)

    print(response.text)


example_url = 'https://www.example.com'
example_url_data = 'https://www.example.com/api/data'

accept_encoding_example(example_url)
content_encoding_example(example_url_data)
user_agent_accept_language_example(example_url)
