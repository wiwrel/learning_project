import asyncio


async def fetch_data():
    reader, writer = await asyncio.open_connection('ya.ru', 80)
    request_header = 'GET / HTTP/1.0\r\nHost: ya.ru\r\n\r\n'
    writer.write(request_header.encode('utf8'))
    await writer.drain()
    response = await reader.read(4096)
    print(response.decode('utf8'))
    writer.close()
    await writer.wait_closed()


asyncio.run(fetch_data())
