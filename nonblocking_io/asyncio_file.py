import asyncio
import aiofiles


async def read_file_async(file_name):
    async with aiofiles.open(file_name, mode='r') as f:
        contents = await f.read()
        print(contents)


asyncio.run(read_file_async('example.txt'))
