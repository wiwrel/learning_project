from time import time


def gen(s):
    for i in s:
        yield i


def gen_filename():
    while True:
        pattern = 'file-{}.jpg'
        t = int(time() * 1000)
        yield pattern.format(str(t))

        sum = 236 + 256
        print(sum)


for i in gen_filename():
    print(i)