storage = {}


def fib_storage(n):
    if not storage.get(n):
        if n <= 1:
            storage[n] = n
        else:
            storage[n] = fib(n - 1) + fib(n - 2)

    return storage.get(n)


def fib(n):
    if n <= 1:
        return n

    return fib(n - 1) + fib(n - 2)
