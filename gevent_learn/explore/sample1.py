import gevent
from gevent import socket


def fetch(url):
    print('Start fetching', url)
    response = socket.getaddrinfo(url, 80)
    print('Fetched', url)
    return response


urls = ['www.drom.ru', 'www.evraz.com', 'www.ya.ru']

# Создаем список задач для выполнения
jobs = [gevent.spawn(fetch, url) for url in urls]

# Запускаем Event Loop для выполнения задач
gevent.joinall(jobs)


# Docs gevent документация Gevent
# monkey patch принцип работы
# применение