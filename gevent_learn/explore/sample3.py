import gevent
from gevent import socket, monkey


def handle_request(conn):
    data = conn.recv(1024)
    print(f"Received data: {data.decode()}")
    conn.send(b"Hello from Gevent!")
    conn.close()


def server():
    server_socket = socket.socket()
    server_socket.bind(("localhost", 8000))
    server_socket.listen(5)

    while True:
        conn, addr = server_socket.accept()
        gevent.spawn(handle_request, conn)


if __name__ == "__main__":
    monkey.patch_all()
    server()
