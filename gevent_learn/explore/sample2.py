from gevent import monkey

monkey.patch_all()
import gevent
import requests


def fetch_url(url):
    print(f"Начало загрузки {url}")
    data = requests.get(url).text
    print(f"{url}: Получено {len(data)} символов")


urls = [
    "https://www.python.org",
    "https://www.google.com",
    "https://www.yahoo.com"
]

# Создаем зеленые потоки
greenlets = [gevent.spawn(fetch_url, url) for url in urls]

# Запускаем и ждем завершения всех зеленых потоков
gevent.joinall(greenlets)
