import gevent
from gevent import socket
urls = ['www.google.com', 'www.example.com', 'www.python.org']
jobs = [gevent.spawn(socket.gethostbyname, url) for url in urls]
_ = gevent.joinall(jobs, timeout=2)
[job.value for job in jobs]

# ---------------------------------------------------
import gevent
from gevent.event import Event
evt = Event()
def setter():
    print('A: Hey wait for me, I have to do something')
    gevent.sleep(3)
    print("Ok, I'm done")
    evt.set()
def waiter():
    print("I'll wait for you")
    evt.wait()  # blocking
    print("It's about time")
def main():
    gevent.joinall([
        gevent.spawn(setter),
        gevent.spawn(waiter),
        gevent.spawn(waiter),
        gevent.spawn(waiter),
        gevent.spawn(waiter),
        gevent.spawn(waiter)
    ])
if __name__ == '__main__': main()

# ----------------------------------
import gevent.monkey
gevent.monkey.patch_socket()
import gevent
import urllib3
import random
def fetch(pid):
    response = urllib3.request('GET', 'https://jsonplaceholder.typicode.com/todos/1')
    json_result = response.json()

    delay = random.choice(range(3))

    gevent.sleep(delay)
    print('Process %s: todos %s. Delay: %s' % (pid, json_result.get('title'), delay))

    return json_result.get('title')
def synchronous():
    for i in range(1,10):
        fetch(i)
def asynchronous():
    threads = []
    for i in range(1, 10):
        threads.append(gevent.spawn(fetch, i))
    gevent.joinall(threads)
print('Synchronous:')
synchronous()
print('Asynchronous:')
asynchronous()
