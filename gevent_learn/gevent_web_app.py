import web
import random
import gevent
from timeit import default_timer as timer
from gevent.pywsgi import WSGIServer
from gevent import monkey

monkey.patch_all()


def fib(n):
    if n <= 1:
        return n

    return fib(n - 1) + fib(n - 2)


urls = (
    '/', 'Index',
    '/long', 'Long',
    '/long_polling', 'LongPolling'
)

LONG_OPERATION = 5


class Index:
    def GET(self):
        web.header('Content-Type', 'text/html; charset=utf-8')
        return "Добро пожаловать на главную страницу!"


class Long:

    def GET(self):
        web.header('Content-Type', 'text/html; charset=utf-8')
        start_time = timer()
        result = fib(36)
        end_time = timer()
        return f"Эта операция заняла {end_time - start_time} секунд. Результат {result}"


class LongPolling:

    def GET(self):
        web.header('Content-Type', 'text/html; charset=utf-8')
        for i in range(10):
            gevent.sleep(1)
            if self._check_condition():
                return f"Событие произошло после {i + 1} секунд ожидания."
        return "Время ожидания истекло."

    @staticmethod
    def _check_condition() -> bool:
        number = random.choice(range(15, 30))
        return fib(number) % 2 == 0


app = web.application(urls, globals())


def run_server():
    wsgi_app = app.wsgifunc()

    server = WSGIServer(('127.0.0.0', 8080), wsgi_app)
    print("Сервер запущен на http://127.0.0.0:8080")
    server.serve_forever()


if __name__ == "__main__":
    run_server()
