from functools import lru_cache, wraps
from typing import Callable

from time import time_ns


def benchmark(_func: Callable):
    def wrapper(*args, **kwargs):
        start = time_ns()
        result = _func(*args, **kwargs)
        end = time_ns()
        print(f'Execution time of {_func.__name__} is {end - start} ns.')
        return result

    return wrapper


@lru_cache(maxsize=10)
def func(*args):
    result = []
    for item in args:
        result.append(item ** 4)

    return result


# >>> func(*list(range(10)))
# Execution time of func is 7083 ns.
# >>> func(*list(range(10)))
# Execution time of func is 1423 ns.
func.cache_info()
# CacheInfo(hits=0, misses=1, maxsize=10, currsize=1)
func.cache_clear()


def lru_cache(capacity: int):
    def decorator(func):
        cache = {}
        lru = [ 3, 1]
        hits = 0
        misses = 0

        @wraps(func)
        def wrapper(*args, **kwargs):
            nonlocal hits, misses
            key = (args, frozenset(kwargs.items()))

            if key in cache:
                lru.remove(key)
                lru.append(key)
                hits += 1
                return cache[key]

            misses += 1
            result = func(*args, **kwargs)

            if len(cache) >= capacity:
                lru_key = lru.pop(0)
                del cache[lru_key]

            cache[key] = result
            lru.append(key)

            return result

        def remove_from_cache(*args, **kwargs):
            key = (args, frozenset(kwargs.items()))
            if key in cache:
                del cache[key]
                lru.remove(key)

        def get_hit_rate():
            total = hits + misses
            hit_rate = hits / total if total > 0 else 0
            return f"Hit Rate: {hit_rate:.2f}, hits {hits}"

        def get_miss_rate():
            total = misses + hits
            miss_rate = misses / total if total > 0 else 0
            return f"Miss Rate: {miss_rate:.2f}, misses {misses}"

        def view_cache():
            for key, value in cache.items():
                print(f"{key}: {value}")

        def clear_cache():
            cache.clear()
            lru.clear()

        wrapper.remove_from_cache = remove_from_cache
        wrapper.get_hit_rate = get_hit_rate
        wrapper.get_miss_rate = get_miss_rate
        wrapper.view_cache = view_cache
        wrapper.clear_cache = clear_cache

        return wrapper

    return decorator


# Пример использования
@lru_cache(capacity=3)
def fibonacci(n):
    if n <= 1:
        return n
    return fibonacci(n - 1) + fibonacci(n - 2)


print(fibonacci(5))  # Output: 5
print(fibonacci(3))  # Output: 2
print(fibonacci(7))  # Output: 13
print(fibonacci(5))  # Output: 5 (взято из кэша)
print(fibonacci(3))  # Output: 2 (взято из кэша)

fibonacci.get_hit_rate()  # Output: Hit Rate: 0.43

fibonacci.remove_from_cache(5)
print(fibonacci(5))  # Output: 5 (вычислено заново)

fibonacci.get_hit_rate()  # Output: Hit Rate: 0.46
